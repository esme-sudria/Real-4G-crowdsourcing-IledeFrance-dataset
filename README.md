## Crowdsourcing compaign description
This work is proposed to provide a new dataset that contains Crowdsourced Network Measurements (CNMs) of Mobile cellular network in Paris reagion (Ile-De-France) from Mars to April 2021. It is composed of passive trace measurements from the "5Gmark" tool. 

The initial dataset (`data_raw`) contains more than `72000` customer-side cellular measurements collected from one of the major French mobile operators, through two mobility modes (`bus` and `train`).

 More than `80 Go` of data was downloaded for  :

- ` 5 Services` (`latency, download, upload, web browsing and video`) 
    
-  `5 Cellular technologies` (`LTE_A, LTE, HSPAP, HSPA, UMTS`). 


## Tool 
For the collection of our own dataset, we utilize version '4.4.1' of the "5Gmark" mobile network monitoring tool for Android devices. The application is installed on two devices :Samsung A10 and Redmi Pro. 9.  It allows to measure the cellular connection of your mobile through `3` modes: `Speed Test`, `Full Test` and `Drive test`. 

In practice, the `Full Test` integrates in addition to the `Speed Test` flow values the measurement of real customer usage (YouTube streaming and web browsing). The `Drive Test` represents a test cycle, set of `Speed Tests` or "Full Tests", which runs automatically with a tests number counter (`5`, `10`, `20`, etc.) and an interval in each test in minutes (by default 0).
 
 
# Repertory description
The structure of the repository is as follows:

   - `data_raw`: Open dataset containing the collected CNMs mesures raw data.
   
   - `data_clean`: Open dataset containing cleaned dataset.

   - `datasets_services`: Open datasets containing `5 Services` (`latency, download, upload, web browsing and video`) .
   
   - `scripts`: Utility scripts to analyze and plot CNMs information.
   
   Please refer to the `README` files in the subfolders for more information and licences.
   
   


## Data details
**A) Service state** :
- OK :        63643
- TIMEOUT :     4486
- FAILED :      231

**B) Mobility patterns** :
- TRAIN :   35258
- CAR :     33102

**C) Services** :
- VIDEO :       31068
- WEB :        14723
- DOWNLOAD :    10373
- UPLOAD :     10293
- LATENCY :     1903


**D) Technologies** :
- LTE :      65517
- HSPAP :      1582
- ISHO4G :    1261


## Acknowledge
   If you use the proposed open traces, please follow the aknowledgement rules in the `README` files in the subfolders.
   
    
## Related publication - Paper to cite
   Amour, L.; Dandoush, A. Crowdsourcing Based Performance Analysis of Mobile User Heterogeneous Services. Electronics 2022, 11, 1011. https://doi.org/10.3390/electronics11071011 

## Authors
- Lamine Amour (Modeling & Digital Technologies department - Engineering school Ivry - ESME Sudria) 

- Abdulhalim Dandoush (Modeling & Digital Technologies department - Engineering school Ivry - ESME Sudria) 

