from statistics import mean

import pandas as pd

###################################################################################
#####################################################################################

### Web site to see  :  https://www.nhc.noaa.gov/gccalc.shtml

data = pd.read_csv("LaDataMOS.csv", sep=";")
print(data)
print("ok")

minLatitude   =  min(data['location_latitude'])
maxLatitude =  max(data['location_latitude'])
minLongitude   =  min(data['location_longitude'])
maxLongitude =  max(data['location_longitude'])

print("  * * * * * * * I)  Dataframe information * * * * * * *  ")
print(" Latitude MIN = ",minLatitude)
print(" Latitude MAX = ",maxLatitude)
print(" LONGITUDE MIN = ",minLongitude)
print(" LONGITUDE MAX = ",maxLongitude)

## Recuperate PARTS
TotalLatitude = maxLatitude - minLatitude
TotalLongitude = maxLongitude - minLongitude


print("  * * * * * * * II)  Total distance - GPS  * * * * * * *  ")
parts_nbr = 16
print('------------------------------------------------------- ')
print(" Distance GPS Latitude = ",TotalLatitude)
print(" Divided on ",parts_nbr," parts = ", TotalLatitude/parts_nbr)
print(" Wide of a part (Latitude) = ", (TotalLatitude/parts_nbr)*111," (KM)")
print('------------------------------------------------------- ')
print(" Distance GPS Latitude = ",TotalLongitude)
print(" Divided on ",parts_nbr," parts = ", TotalLongitude/parts_nbr)
print(" Wide of a part (Longitude) = ", (TotalLongitude/parts_nbr)*73," (KM)")
print('------------------------------------------------------- ')


#################################################################

print("  * * * * * * * III)  Former les traces  * * * * * * *  ")
i_index = 1
j_index = 1

IndexPart = i_index + j_index

PAS_Latitude = TotalLatitude/parts_nbr
PAS_Longitude = TotalLongitude/parts_nbr



df = pd.Series(data.id_QSCycle)
list_unique_CYCLES = pd.unique(df)
#print(len(list_unique_CYCLES))

valMOSvideo = 0

TAB_best_one = []
TAB_best_all= []

for ii in range(0, len(list_unique_CYCLES)):
    valMOSvideo = 0
    valMOSweb = 0
    valMOSdownload = 0
    dataSelectCYCLES = data[(data['id_QSCycle'] == list_unique_CYCLES[ii])  ]
    print(" ID cycle ===  (",list_unique_CYCLES[ii],  ")")
    data1 = dataSelectCYCLES[(dataSelectCYCLES['protocol'] == 'STREAM')]
    data2 = dataSelectCYCLES[ (dataSelectCYCLES['protocol'] == 'WEB')   ]
    data3 = dataSelectCYCLES[(dataSelectCYCLES['protocol'] == 'DOWNLOAD')]
    print("___________________________________________________________")
    try:
        print("MOS VIDEO     == ", max(data1.MOSclass))
        valMOSvideo = max(data1.MOSclass)
    except:
        print("ERREUR video")


    print("___________________________________________________________")

    try:
        print("MOS WEB     == ", max(data2.MOSclass))
        valMOSweb = max(data2.MOSclass)
    except:
        print("ERREUR WEB")

    print("___________________________________________________________")

    try:
        print("MOS DOWNLOAD     == ", max(data3.MOSclass))
        valMOSdownload = max(data3.MOSclass)
    except:
        print("ERREUR DOWNLOAD")


    if ((valMOSvideo>valMOSweb)|(valMOSvideo>valMOSdownload)):
        TAB_best_one.append(list_unique_CYCLES[ii])

    if ((valMOSvideo>valMOSweb)& (valMOSvideo>valMOSdownload)):
        TAB_best_all.append(list_unique_CYCLES[ii])

    print("*******************************************************************************************")
    print("*******************************************************************************************")

print("__________________________________")
print(len(TAB_best_one))
print("__________________________________")
print(len(TAB_best_all))
print("__________________________________")

for k in range(0, len(TAB_best_all)):
    print(TAB_best_all[k])


# assign data of lists.
dataResultOne = pd.DataFrame(TAB_best_one)
dataResultOne.to_csv('dataCellOne.csv', index = False, header = True, sep = ";")


dataResultAll = pd.DataFrame(TAB_best_all)
dataResultAll.to_csv('dataCellAll.csv', index = False, header = True, sep = ";")
