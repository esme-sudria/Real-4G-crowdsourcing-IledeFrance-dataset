import pandas as pd

###################################################################################

# Function definition
def FunctionGetMOSfromBitrate(bitrate_video):
    dataRateBR = pd.read_csv("used_rate_bitrate.csv", sep=";")
    x = dataRateBR['bitrate_avg']
    mos_values = dataRateBR['MOS']
    for i in range(1, len(x) - 1):

        if (bitrate_video > 11000):
            return (5)
        else:
            nextItem = i + 1
            if (bitrate_video >= x[i]) and (bitrate_video < x[nextItem]):
                return mos_values[i]


def FunctionGetMOSDOWNLOADfromBitrate(BitrateDonwl):
    dataRateDwonload = pd.read_csv("used_rate_download.csv", sep=";")
    x = dataRateDwonload['bitrate_avg']
    mos_values = dataRateDwonload['MOS']
    for i in range(1, len(x) - 1):
        if (BitrateDonwl > 350):
            return (5)
        else:
            nextItem = i + 1
            if (BitrateDonwl >= x[i]) and (BitrateDonwl < x[nextItem]):
                return mos_values[i]


#####################################################################################

### Web site to see  :  https://www.nhc.noaa.gov/gccalc.shtml

data = pd.read_csv("step0-DataAll.csv", sep=";")
print(data)
print("ok")


data = data[['id','id_QSCycle',
             'start_date','cellular_cid',
                      'code_national',
                      'name',
                      'location_latitude',
                      'location_longitude',
                       'network_techno',
                      'status',
                      'launch_duration',
                      'bitrate_traffic',
                      'trace_location_speed',
                      'trace_cellular_lte_rsrp',
                      'trace_cellular_lte_rsrq',
                      'trace_cellular_lte_rssnr',
                      'trace_cellular_lte_cqi',
                      'trace_cellular_lte_dbm',
                      'trace_cellular_lte_asu',
                       'trace_cellular_lte_level',
                      'trace_cellular_lte_signal_strength',
                      'protocol','user_location_type','brand']]


data = data[(data['protocol'] == 'STREAM') | (data['protocol'] == 'WEB') | (data['protocol'] == 'DOWNLOAD')  ]



print("  * * * * * * * III)  Former les traces  * * * * * * *  ")
i_index = 1
j_index = 1
TAB_MOS = []

print("NB examples is  ==== ", len(data['bitrate_traffic']))
for i in range(0, len(data['bitrate_traffic'])):
    print(" sample i = ",i,"  is on  process _______ " )
    if(data.iloc[i]['protocol'] == "STREAM"):
        #### VIDEO
        MOS = FunctionGetMOSfromBitrate(data.iloc[i]['bitrate_traffic'])
        TAB_MOS.append(MOS)
        #print("Video   ............. 1")
    else:
        if (data.iloc[i]['protocol'] == "DOWNLOAD"):
            #### DOWNLOAD
            MOS = FunctionGetMOSDOWNLOADfromBitrate(data.iloc[i]['bitrate_traffic'])
            TAB_MOS.append(MOS)
            #print("Download    .......................... 2")
        else:
            #### WEB
            𝐷𝐷 = (data.iloc[i]['launch_duration']) / 1000  # in seconds
            MOSweb = 5 - (578 / (1 + ((11.77 + 22.61 / 𝐷𝐷) * (11.77 + 22.61 / 𝐷𝐷))))
            TAB_MOS.append(MOSweb)
            #print("web   ........................................................ 3")



print(TAB_MOS)
data['MOS'] = TAB_MOS
print(" MOS calculation is --OK--")
print("  * * * * * * * VII -  Export CSV file to third module * * * * * * *  ")
data.to_csv('step1-DataMOS.csv', index=False, header=True, sep=";")

'''
print("  * * * * * * *  CALCULATE MOS classe  * * * * * * *  ")
TABmosPredictClass5 = []
for i in range(0, len(data['bitrate_traffic'])):
    if (TAB_MOS.iloc[i]['MOS'] < 1.5):
        TABmosPredictClass5.append(1)
    else:
        if (TAB_MOS.iloc[i]['MOS'] < 2.5):
            TABmosPredictClass5.append(2)
        else:
            if (TAB_MOS.iloc[i]['MOS'] < 3.5):
                TABmosPredictClass5.append(3)
            else:
                if (TAB_MOS.iloc[i]['MOS'] < 4.5):
                    TABmosPredictClass5.append(4)
                else:
                    TABmosPredictClass5.append(5)
data["MOSclass"] = TABmosPredictClass5



print(" MOS calculation is --OK--")
print("  * * * * * * * VII -  Export CSV file to third module * * * * * * *  ")
data.to_csv('step1-DataMOS.csv', index=False, header=True, sep=";")
'''