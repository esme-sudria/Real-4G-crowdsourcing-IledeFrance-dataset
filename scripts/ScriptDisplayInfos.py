import pandas as pd

###################################################################################
#####################################################################################

### Web site to see  :  https://www.nhc.noaa.gov/gccalc.shtml

data = pd.read_csv("step1-DataMOS.csv", sep=";")
print(data)

print("  * * * * * * * I)  Dataframe information * * * * * * *  ")
print(" 1) Statistics  _  describe ()  ")
print(data.describe())
print("______________________________________")
print(" 2) Informaton (Column, Non-Null, Count, Dtype)  _  info ()  ")
print(data.info())


print("  --------------------1) Status ------------------------------  164426")
print(data.status.value_counts()/164426)
print("  ------------------------------------------------------------  ")
print("  --------------------2) protocol ------------------------------  ")
print(data.protocol.value_counts()/164426)
print("  ------------------------------------------------------------  ")
print("  --------------------3) user_location_type ------------------------------  ")
print(data.user_location_type.value_counts())
print("  ------------------------------------------------------------  ")
print("  --------------------4) brand ------------------------------  ")
print(data.brand.value_counts()/164426)
print("  ------------------------------------------------------------  ")
print("  --------------------5) network_techno ------------------------------  ")
print(data.network_techno.value_counts()/164426)
print("  ------------------------------------------------------------  ")
print("  --------------------6) MOS classe ------------------------------  ")
print(data.Classe.value_counts()/164426)
print("  ------------------------------------------------------------  ")
print("  --------------------6) STATS ------------------------------  ")
print("  ------------------------------------------------------------  ")


print("  * * * * * * * VII -  Export CSV file to third module * * * * * * *  ")
#data.to_csv('step0-DataAll.csv', index=False, header=True, sep=";")

###############
