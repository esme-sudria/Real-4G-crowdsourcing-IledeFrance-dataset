import pandas as pd

###################################################################################

# Function definition
def FunctionGetMOSfromBitrate(bitrate_video):
    dataRateBR = pd.read_csv("used_rate_bitrate.csv", sep=";")
    x = dataRateBR['bitrate_avg']
    mos_values = dataRateBR['MOS']
    for i in range(1, len(x) - 1):

        if (bitrate_video > 11000):
            return (5)
        else:
            nextItem = i + 1
            if (bitrate_video >= x[i]) and (bitrate_video < x[nextItem]):
                return mos_values[i]


def FunctionGetMOSDOWNLOADfromBitrate(BitrateDonwl):
    dataRateDwonload = pd.read_csv("used_rate_download.csv", sep=";")
    x = dataRateDwonload['bitrate_avg']
    mos_values = dataRateDwonload['MOS']
    for i in range(1, len(x) - 1):
        if (BitrateDonwl > 350):
            return (5)
        else:
            nextItem = i + 1
            if (BitrateDonwl >= x[i]) and (BitrateDonwl < x[nextItem]):
                return mos_values[i]


#####################################################################################

### Web site to see  :  https://www.nhc.noaa.gov/gccalc.shtml

data = pd.read_csv("dataF.csv", sep=";")
print(data)
print("ok")


data = data[['id','id_QSCycle',
             'start_date','cellular_cid',
                      'code_national',
                      'name',
                      'location_latitude',
                      'location_longitude',
                       'network_techno',
                      'status',
                      'launch_duration',
                      'bitrate_traffic',
                      'trace_location_speed',
                      'trace_cellular_lte_rsrp',
                      'trace_cellular_lte_rsrq',
                      'trace_cellular_lte_rssnr',
                      'trace_cellular_lte_cqi',
                      'trace_cellular_lte_dbm',
                      'trace_cellular_lte_asu',
                       'trace_cellular_lte_level',
                      'trace_cellular_lte_signal_strength',
                      'protocol','user_location_type','brand']]


#################################################################

# Filter technology -
data = data[ ((data['status'] == 'OK')|
              (data['status'] == 'TIMEOUT')|
              (data['status'] == 'FAILED')
              ) ]


# Filter user location - mobility mode
data = data[ ((data['user_location_type'] == 'CAR')|(data['user_location_type'] == 'TRAIN'))   ]


# Filter technology -
data = data[ ((data['network_techno'] == 'LTE')|
              (data['network_techno'] == 'HSPAP')|
              (data['network_techno'] == 'ISHO4G')|
              (data['network_techno'] == 'UMTS')|
              (data['network_techno'] == 'EDGE')|
              (data['network_techno'] == 'ALL3G')
              ) ]

print("  * * * * * * * I)  Dataframe information * * * * * * *  ")
print(" 1) Statistics  _  describe ()  ")
print(data.describe())
print("______________________________________")
print(" 2) Informaton (Column, Non-Null, Count, Dtype)  _  info ()  ")
print(data.info())


print("  --------------------1) Status ------------------------------  ")
print(data.status.value_counts())
print("  ------------------------------------------------------------  ")
print("  --------------------2) protocol ------------------------------  ")
print(data.protocol.value_counts())
print("  ------------------------------------------------------------  ")
print("  --------------------3) user_location_type ------------------------------  ")
print(data.user_location_type.value_counts())
print("  ------------------------------------------------------------  ")
print("  --------------------4) brand ------------------------------  ")
print(data.brand.value_counts())
print("  ------------------------------------------------------------  ")
print("  --------------------5) network_techno ------------------------------  ")
print(data.network_techno.value_counts())
print("  ------------------------------------------------------------  ")


print("  * * * * * * * VII -  Export CSV file to third module * * * * * * *  ")
data.to_csv('step0-DataAll.csv', index=False, header=True, sep=";")

###############
